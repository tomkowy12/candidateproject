package pl.tk.CandidateTask.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "customers_data")
public class CustomerData {

    private static final long MILLISECONDS_IN_MONTH = 2592000000L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "info_as_of_date")
    private Date infoAsOfDate; // info_as_of_date

    @Column(name = "customer_id")
    private long customerId; // customer_id

    @Column(name = "customer_name")
    private String customerName; // customer_name

    @Column(name = "customer_start_date")
    private Date customerStartDate; // customer_start_date

    @Column(name = "customer_type")
    private CustomerType customerType; // customer_type

    @Column(name = "customer_income")
    private double customerIncome; // customer_income

    @Column(name = "customer_risk_class")
    private CustomerRiskClass customerRiskClass; // customer_risk_class

    @Column(name = "customer_business_type")
    private CustomerBusinessType customerBusinessType; // customer_business_type

    @Column(name = "r1")
    private double r1;

    @Column(name = "r2")
    private double r2;

    @Transient
    private Double averageCustomerIncome;

    public CustomerData() {

    }

    public CustomerData(Date infoAsOfDate, long customerId, String customerName, Date customerStartDate,
                           CustomerType customerType, double customerIncome, CustomerRiskClass customerRiskClass,
                           CustomerBusinessType customerBusinessType) {
        this.infoAsOfDate = infoAsOfDate;
        this.customerId = customerId;
        this.customerName = customerName;
        this.customerStartDate = customerStartDate;
        this.customerType = customerType;
        this.customerIncome = customerIncome;
        this.customerRiskClass = customerRiskClass;
        this.customerBusinessType = customerBusinessType;
    }

    public long getId() {
        return id;
    }

    public Date getInfoAsOfDate() {
        return infoAsOfDate;
    }

    public long getCustomerId() {
        return customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public Date getCustomerStartDate() {
        return customerStartDate;
    }

    public CustomerType getCustomerType() {
        return customerType;
    }

    public double getCustomerIncome() {
        return customerIncome;
    }

    public CustomerRiskClass getCustomerRiskClass() {
        return customerRiskClass;
    }

    public CustomerBusinessType getCustomerBusinessType() {
        return customerBusinessType;
    }

    public double getR1() {
        return r1;
    }

    public void setR1(double r1) {
        this.r1 = r1;
    }

    public double getR2() {
        return r2;
    }

    public void setR2(double r2) {
        this.r2 = r2;
    }

    @Override
    public String toString() {
        return "CustomerData{" +
                "infoAsOfDate=" + this.getInfoAsOfDate() +
                ", customerId=" + this.getCustomerId() +
                ", customerName='" + this.getCustomerName() + '\'' +
                ", customerStartDate=" + this.getCustomerStartDate() +
                ", customerType=" + this.getCustomerType() +
                ", customerIncome=" + this.getCustomerIncome() +
                ", customerRiskClass=" + this.getCustomerRiskClass() +
                ", customerBusinessType=" + this.getCustomerBusinessType() +
                ", r1=" + r1 +
                ", r2=" + r2 +
                '}';
    }
}
