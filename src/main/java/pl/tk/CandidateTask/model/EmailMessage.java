package pl.tk.CandidateTask.model;

public class EmailMessage {
    private final String message;
    private final String topic;
    private final String address;

    public EmailMessage(String message, String topic, String address) {
        this.message = message;
        this.topic = topic;
        this.address = address;
    }

    public String getMessage() {
        return message;
    }

    public String getTopic() {
        return topic;
    }

    public String getAddress() {
        return address;
    }
}
