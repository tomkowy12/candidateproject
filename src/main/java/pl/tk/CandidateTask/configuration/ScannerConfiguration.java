package pl.tk.CandidateTask.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("file:src/main/resources/application.properties")
public class ScannerConfiguration {

    @Value("${customers.data.path}")
    private String dataPath;

    @Value("${customers.scanning.interval:1}")
    private long scanningIntervalInMinutes;

    public ScannerConfiguration() {

    }

    public String getDataPath() {
        return dataPath;
    }

    public long getScanningIntervalInMinutes() {
        return scanningIntervalInMinutes;
    }
}
