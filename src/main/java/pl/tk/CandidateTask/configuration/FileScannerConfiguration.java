package pl.tk.CandidateTask.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.devtools.filewatch.FileSystemWatcher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import pl.tk.CandidateTask.service.FileScannerService;

import javax.annotation.PreDestroy;
import java.io.File;
import java.time.Duration;

@Configuration
@PropertySource("file:src/main/resources/application.properties")
public class FileScannerConfiguration {

    @Value("${customers.data.path}")
    private String dataPath;

    @Value("${customers.scanning.interval:1}")
    private long scanningIntervalInMinutes;

    @Autowired
    FileScannerService fileScannerService;

    @Bean
    public FileSystemWatcher fileSystemWatcher() {
        FileSystemWatcher fileSystemWatcher = new FileSystemWatcher(true, Duration.ofMillis(5000L), Duration.ofMillis(3000L));
        fileSystemWatcher.addSourceDirectory(new File(dataPath));
        fileSystemWatcher.addListener(fileScannerService);
        fileSystemWatcher.start();
        //TODO: log watcher start
        return fileSystemWatcher;
    }

    @PreDestroy
    public void onDestroy() {
        fileSystemWatcher().stop();
    }
}
