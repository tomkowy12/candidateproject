package pl.tk.CandidateTask.riskCounting;

import pl.tk.CandidateTask.model.CustomerBusinessType;
import pl.tk.CandidateTask.model.CustomerData;

public class TypeA1Strategy implements RiskCountingStrategy {
    @Override
    public double calculateR1(CustomerData customerData, Double averageCustomerIncome) {
        return (customerData.getCustomerIncome() / 10) *
                this.calculateF1(customerData.getCustomerIncome(), averageCustomerIncome);
    }

    @Override
    public double calculateR2(CustomerData customerData) {
        return customerData.getCustomerIncome() / 1000 * calculateF2(customerData.getCustomerBusinessType());
    }

    /* 3 przypadki
        * średnia wartość customer_income z ostatnich 30 dni < 80% aktualniej warotści customer_income - return 0.3
        * średnia wartość customer_income z ostatnich 30 dni > 80% aktualnej wartości customer_income - return 0.2
        * nie ma wpisów dla klienta o tym id - 0.25
        * jeśli nie ma wpisów w bazie to zostawić null averageCustomerIncome
     */
    private double calculateF1(double customerIncome, Double averageCustomerIncome) {
        if (averageCustomerIncome == null)
            return 0.25;
        if (averageCustomerIncome < customerIncome * 0.8) {
            return 0.3;
        }
        else
            return 0.2;
    }

    private double calculateF2(CustomerBusinessType customerBusinessType) {
        switch (customerBusinessType) {
            case BT_1: case BR_2:
                return 0.5;
            case BR_3:
                return 0.8;
            default:
                return 0.9;
        }
    }
}
