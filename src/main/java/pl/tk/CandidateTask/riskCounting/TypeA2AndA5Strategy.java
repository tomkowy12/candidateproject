package pl.tk.CandidateTask.riskCounting;

import pl.tk.CandidateTask.model.CustomerData;

public class TypeA2AndA5Strategy implements RiskCountingStrategy {
    @Override
    public double calculateR1(CustomerData customerData, Double averageCustomerIncome) {
        return customerData.getCustomerIncome() / 10;
    }

    @Override
    public double calculateR2(CustomerData customerData) {
        return customerData.getCustomerIncome() / 100;
    }
}
