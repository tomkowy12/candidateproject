package pl.tk.CandidateTask.riskCounting;

import pl.tk.CandidateTask.model.CustomerBusinessType;
import pl.tk.CandidateTask.model.CustomerData;

public class TypeA5SecondCaseStrategy implements RiskCountingStrategy {
    @Override
    public double calculateR1(CustomerData customerData, Double averageCustomerIncome) {
        return customerData.getCustomerIncome() / 10 *
                this.calculateF1(customerData.getCustomerBusinessType());
    }

    @Override
    public double calculateR2(CustomerData customerData) {
        return customerData.getCustomerIncome() / 100;
    }

    private double calculateF1(CustomerBusinessType customerBusinessType) {
        if (customerBusinessType == CustomerBusinessType.BR_3) {
            return 0.1;
        }
        return 0.2;
    }
}
