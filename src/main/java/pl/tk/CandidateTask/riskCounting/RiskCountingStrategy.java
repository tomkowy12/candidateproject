package pl.tk.CandidateTask.riskCounting;

import pl.tk.CandidateTask.model.CustomerData;

public interface RiskCountingStrategy {
    double calculateR1(CustomerData customerData, Double averageCustomerIncome);
    double calculateR2(CustomerData customerData);
}
