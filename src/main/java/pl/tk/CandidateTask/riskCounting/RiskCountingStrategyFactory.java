package pl.tk.CandidateTask.riskCounting;

import pl.tk.CandidateTask.model.CustomerData;
import pl.tk.CandidateTask.model.CustomerRiskClass;
import pl.tk.CandidateTask.model.CustomerType;

public class RiskCountingStrategyFactory {
    public static RiskCountingStrategy getRiskStrategy(CustomerData customerData) {
        if (customerData.getCustomerType() == CustomerType.TYPE_A1) {
            return new TypeA1Strategy();
        }
        else if (customerData.getCustomerType() == CustomerType.TYPE_A5 && customerData.getCustomerRiskClass() == CustomerRiskClass.A3) {
            return new TypeA5SecondCaseStrategy();
        }
        else /*if (customerData.getCustomerType() == CustomerType.TYPE_A5 || customerData.getCustomerType() == CustomerType.TYPE_A2)*/ {
            return new TypeA2AndA5Strategy();
        }
    }
}
