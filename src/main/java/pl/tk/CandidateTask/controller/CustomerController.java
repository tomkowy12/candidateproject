package pl.tk.CandidateTask.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.tk.CandidateTask.model.*;
import pl.tk.CandidateTask.service.CustomerService;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/customer/")
public class CustomerController {

    @Autowired
    private CustomerService customerservice;

    @GetMapping("/{id}")
    public CustomerData getNewestClientDataById(@PathVariable(value = "id") Long customerId) {
        return this.customerservice.findFirstByCustomerIdOrderByInfoAsOfDateDesc(customerId);
    }

    @GetMapping("/{id}/date/{date}")
    public List<CustomerData> getCustomersDataByDay(@PathVariable(value = "id") Long customerId,
                                                    @PathVariable(value = "date") String date) {
        return this.customerservice.findCustomersDataByDayAndId(customerId, date); // TODO: na razie zwraca wszystkich
    }

}
