package pl.tk.CandidateTask.mailNotifications;

import pl.tk.CandidateTask.model.CustomerData;
import pl.tk.CandidateTask.model.EmailMessage;

import java.util.List;

public interface MailNotificationStrategy {
    List<EmailMessage> getEmailMessage(CustomerData customerData);
}
