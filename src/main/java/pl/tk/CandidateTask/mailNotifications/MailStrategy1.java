package pl.tk.CandidateTask.mailNotifications;

import pl.tk.CandidateTask.model.CustomerData;
import pl.tk.CandidateTask.model.EmailMessage;

import java.util.ArrayList;
import java.util.List;

public class MailStrategy1 implements MailNotificationStrategy {
    private static final String EMAIL_MESSAGE = "Uprzejmie informuję, że zmieniła się klasa ryzyka dla klienta ";
    private static final String EMAIL_TOPIC = "Informacja dla klienta";
    private static final List<String> ADDRESSES = List.of(new String[]{"dyrektorjednostki@fikcyjnafirma.com",
            "koordynatorkoordynatorow@fikcyjnafirma.com"});

    @Override
    public List<EmailMessage> getEmailMessage(CustomerData customerData) {
        String message = EMAIL_MESSAGE + customerData.getCustomerId();
        List<EmailMessage> emails = new ArrayList<>();
        for (String address : ADDRESSES) {
            emails.add(new EmailMessage(message, EMAIL_TOPIC, address));
        }
        return emails;
    }
}
