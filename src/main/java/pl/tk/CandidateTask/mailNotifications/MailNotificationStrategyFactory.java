package pl.tk.CandidateTask.mailNotifications;

import pl.tk.CandidateTask.model.CustomerBusinessType;
import pl.tk.CandidateTask.model.CustomerData;
import pl.tk.CandidateTask.model.CustomerType;

public class MailNotificationStrategyFactory {
    public static MailNotificationStrategy getMailNotificationStrategy(CustomerData customerData) {
        if (customerData.getCustomerType() == CustomerType.TYPE_A1 ||
                customerData.getCustomerType() == CustomerType.TYPE_A5) {
            return new MailStrategy1();
        } else if (customerData.getCustomerType() == CustomerType.TYPE_A2 &&
                customerData.getCustomerBusinessType() == CustomerBusinessType.BR_2) {
            return new MailStrategy2();
        }
        return null;
    }
}
