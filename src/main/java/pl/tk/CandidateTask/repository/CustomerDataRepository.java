package pl.tk.CandidateTask.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.tk.CandidateTask.model.CustomerData;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Repository
public interface CustomerDataRepository extends JpaRepository<CustomerData, Long> {

    CustomerData findFirstByCustomerIdOrderByInfoAsOfDateDesc(long customerId);

    @Query("SELECT AVG(c.customerIncome) FROM CustomerData c WHERE c.customerId = ?1 AND c.infoAsOfDate >= ?2 AND c.infoAsOfDate <= NOW()")
    Double findAverageCustomerIncomeForLast30Days(long customerId, Timestamp timestampAfter);

    List<CustomerData> findByCustomerIdEqualsAndInfoAsOfDateBetween(long customerId, Date date1, Date date2);
}
