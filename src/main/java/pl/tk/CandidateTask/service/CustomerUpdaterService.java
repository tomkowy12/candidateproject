package pl.tk.CandidateTask.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.devtools.filewatch.ChangedFile;
import org.springframework.boot.devtools.filewatch.ChangedFiles;
import org.springframework.boot.devtools.filewatch.FileChangeListener;
import org.springframework.stereotype.Service;
import pl.tk.CandidateTask.mailNotifications.MailNotificationStrategy;
import pl.tk.CandidateTask.mailNotifications.MailNotificationStrategyFactory;
import pl.tk.CandidateTask.model.*;
import pl.tk.CandidateTask.riskCounting.RiskCountingStrategy;
import pl.tk.CandidateTask.riskCounting.RiskCountingStrategyFactory;
import pl.tk.CandidateTask.riskCounting.TypeA1Strategy;
import pl.tk.CandidateTask.repository.CustomerDataRepository;

import javax.annotation.PostConstruct;
import java.io.*;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Service
public class CustomerUpdaterService implements FileChangeListener {

    private static final long MILLISECONDS_IN_MONTH = 2592000000L;
    private static final String DATE_PATTERN = "yyyy-MM-dd";

    @Autowired
    FileScannerService fileScannerService;

    @Autowired
    CustomerDataRepository customerDataRepository;

    @Autowired
    MailService mailService;

    public CustomerUpdaterService() {

    }

    @PostConstruct
    private void subscribeToFileScanner() {
        fileScannerService.register(this);
    }

    @Override
    public void onChange(Set<ChangedFiles> changeSet) {
        for (ChangedFiles cfiles : changeSet) {
            for (ChangedFile cfile: cfiles.getFiles()) {
                if (cfile.getType().equals(ChangedFile.Type.ADD) && !isLocked(cfile.getFile().toPath())) {
                    update(getCustomerDataFromCsv(cfile.getFile()));
                }
            }
        }
    }

    public void updateCustomers(List<CustomerData> customersData) {
        for (CustomerData customerData : customersData) {
            prepareCustomerData(customerData);

            boolean isCustomerRiskChanged = checkIfCustomerRiskChanged(customerData);
            if(isCustomerRiskChanged) {
                sendNotificationForCustomerData(customerData);
            }

            customerDataRepository.save(customerData);
        }
    }

    private void sendNotificationForCustomerData(CustomerData customerData) {
        MailNotificationStrategy mailNotificationStrategy =
                MailNotificationStrategyFactory.getMailNotificationStrategy(customerData);
        if (mailNotificationStrategy == null)
            return;
        for (EmailMessage emailMessage : mailNotificationStrategy.getEmailMessage(customerData))
            mailService.send(emailMessage);
    }

    private boolean checkIfCustomerRiskChanged(CustomerData customerData) {
        CustomerData oldCustomerData = customerDataRepository.findFirstByCustomerIdOrderByInfoAsOfDateDesc(customerData.getCustomerId());
        if (oldCustomerData == null)
            return false;
        return oldCustomerData.getCustomerRiskClass() == customerData.getCustomerRiskClass();
    }

    private void prepareCustomerData(CustomerData customerData) {
        Double averageCustomerIncome = null;
        RiskCountingStrategy riskCountingStrategy = RiskCountingStrategyFactory.getRiskStrategy(customerData);
        if (riskCountingStrategy instanceof TypeA1Strategy) {
            averageCustomerIncome = customerDataRepository.findAverageCustomerIncomeForLast30Days(
                    customerData.getCustomerId(),
                    new Timestamp(System.currentTimeMillis() - MILLISECONDS_IN_MONTH));
        }
        customerData.setR1(riskCountingStrategy.calculateR1(customerData, averageCustomerIncome));
        customerData.setR2(riskCountingStrategy.calculateR2(customerData));
    }

    private void update(List<CustomerData> customersData) {
            updateCustomers(customersData);
    }

    private boolean isLocked(Path path) {
        try (FileChannel ch = FileChannel.open(path, StandardOpenOption.WRITE); FileLock lock = ch.tryLock()) {
            return lock == null;
        } catch (IOException e) {
            return true;
        }
    }

    private List<CustomerData> getCustomerDataFromCsv(File file) {
        final List<CustomerData> customers = new ArrayList<>();
        String line;
        try
        {
            BufferedReader br = new BufferedReader(new FileReader(file));
            while ((line = br.readLine()) != null)
            {
                CustomerData parsedCustomersData = parseLineToCustomerData(line);
                if (parsedCustomersData != null) {
                    customers.add(parsedCustomersData);
                }
            }
            return customers;
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
            //TODO: log: file was deleted or moved
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            //TODO: log: something went wrong with file reading
            return null;
        }
    }

    private CustomerData parseLineToCustomerData(String line) {
        String[] customerDataRaw = line.split(",");
        CustomerData customerData;
        try {
            Date infoAsOfDate = new SimpleDateFormat(DATE_PATTERN).parse(customerDataRaw[0]);
            long customerId = Long.getLong(customerDataRaw[1]);
            Date customerStartDate = new SimpleDateFormat(DATE_PATTERN).parse(customerDataRaw[3]);
            CustomerType customerType = CustomerType.valueOf(customerDataRaw[4]);
            double customerIncome = Double.parseDouble(customerDataRaw[5]);
            CustomerRiskClass customerRiskClass = CustomerRiskClass.valueOf(customerDataRaw[6]);
            CustomerBusinessType customerBusinessType = CustomerBusinessType.valueOf(customerDataRaw[7]);
            customerData = new CustomerData(
                    infoAsOfDate, customerId, customerDataRaw[2], customerStartDate, customerType, customerIncome,
                    customerRiskClass, customerBusinessType);
            return customerData;

        } catch (ParseException e) {
            // TODO: log problem with parsing file content
            return null;
        }
    }

}
