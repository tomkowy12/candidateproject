package pl.tk.CandidateTask.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.tk.CandidateTask.model.*;
import pl.tk.CandidateTask.repository.CustomerDataRepository;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class CustomerService {

    @Autowired
    private CustomerDataRepository customerDataRepository;

    private static final String DATE_PATTERN = "yyyy-MM-dd";
    private static final long MILLISECONDS_IN_DAY = 86400000L;

    public CustomerService() {

    }

    public CustomerData findFirstByCustomerIdOrderByInfoAsOfDateDesc(long customerId) {
        return customerDataRepository.findFirstByCustomerIdOrderByInfoAsOfDateDesc(customerId);
    }

    public List<CustomerData> findAll() {
        return customerDataRepository.findAll();
    }

    public void saveCustomerData(CustomerData customerData) {
        customerDataRepository.save(customerData);
    }

    public double findAverageCustomerIncomeForLast30Days(long customerId) {
        return customerDataRepository.findAverageCustomerIncomeForLast30Days(customerId, new Timestamp(System.currentTimeMillis() - 2592000000L));
    }

    public List<CustomerData> findCustomersDataByDayAndId(long id, String dateStr) {
        Date date1;
        Date date2;
        try {
            date1 = new SimpleDateFormat(DATE_PATTERN).parse(dateStr);
            date2 = new Date(date1.getTime() + MILLISECONDS_IN_DAY);
            return customerDataRepository.findByCustomerIdEqualsAndInfoAsOfDateBetween(id, date1, date2);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}
