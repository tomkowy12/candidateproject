package pl.tk.CandidateTask.service;

import org.springframework.boot.devtools.filewatch.ChangedFile;
import org.springframework.boot.devtools.filewatch.ChangedFiles;
import org.springframework.boot.devtools.filewatch.FileChangeListener;
import org.springframework.stereotype.Service;
import pl.tk.CandidateTask.model.*;

import java.io.*;
import java.util.Date;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class FileScannerService implements FileChangeListener {

    private static final String DATE_PATTERN = "yyyy-MM-dd";

    private final List<CustomerUpdaterService> updaters;

    public FileScannerService() {
        updaters = new ArrayList<>();
    }

    public void register(CustomerUpdaterService updater) {
        updaters.add(updater);
    }

    @Override
    public void onChange(Set<ChangedFiles> changeSet) {
        for (ChangedFiles cfiles : changeSet) {
            for (ChangedFile cfile: cfiles.getFiles()) {
                if (cfile.getType().equals(ChangedFile.Type.ADD) && !isLocked(cfile.getFile().toPath())) {
                    update(getCustomerDataFromCsv(cfile.getFile()));
                }
            }
        }
    }

    private void update(List<CustomerData> customersData) {
        for (CustomerUpdaterService updater : updaters) {
            updater.updateCustomers(customersData);
        }
    }

    private boolean isLocked(Path path) {
        try (FileChannel ch = FileChannel.open(path, StandardOpenOption.WRITE); FileLock lock = ch.tryLock()) {
            return lock == null;
        } catch (IOException e) {
            return true;
        }
    }

    private List<CustomerData> getCustomerDataFromCsv(File file) {
        final List<CustomerData> customers = new ArrayList<>();
        String line;
        try
        {
            BufferedReader br = new BufferedReader(new FileReader(file));
            while ((line = br.readLine()) != null)
            {
                CustomerData parsedCustomersData = parseLineToCustomerData(line);
                if (parsedCustomersData != null) {
                    customers.add(parsedCustomersData);
                }
            }
            return customers;
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
            //TODO: log: file was deleted or moved
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            //TODO: log: something went wrong with file reading
            return null;
        }
    }

    private CustomerData parseLineToCustomerData(String line) {
        String[] customerDataRaw = line.split(",");
        CustomerData customerData;
        try {
            Date infoAsOfDate = new SimpleDateFormat(DATE_PATTERN).parse(customerDataRaw[0]);
            long customerId = Long.parseLong(customerDataRaw[1]);
            Date customerStartDate = new SimpleDateFormat(DATE_PATTERN).parse(customerDataRaw[3]);
            CustomerType customerType = CustomerType.valueOf(customerDataRaw[4]);
            double customerIncome = Double.parseDouble(customerDataRaw[5]);
            CustomerRiskClass customerRiskClass = CustomerRiskClass.valueOf(customerDataRaw[6]);
            CustomerBusinessType customerBusinessType = CustomerBusinessType.valueOf(customerDataRaw[7]);
            customerData = new CustomerData(
                    infoAsOfDate, customerId, customerDataRaw[2], customerStartDate, customerType, customerIncome,
                    customerRiskClass, customerBusinessType);
            return customerData;

        } catch (ParseException e) {
            // TODO: log problem with parsing file content
            return null;
        } catch (IllegalArgumentException e) {
            // TODO: log wrong (or new) type of data in enum, log message
            return null;
        }
    }
}
