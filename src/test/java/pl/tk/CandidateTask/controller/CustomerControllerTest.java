package pl.tk.CandidateTask.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.tk.CandidateTask.model.CustomerBusinessType;
import pl.tk.CandidateTask.model.CustomerData;
import pl.tk.CandidateTask.model.CustomerRiskClass;
import pl.tk.CandidateTask.model.CustomerType;
import pl.tk.CandidateTask.service.CustomerService;

import java.util.Date;

import static org.assertj.core.internal.bytebuddy.matcher.ElementMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
//@SpringBootTest(classes = CustomerController.class)
@TestPropertySource(locations = "classpath:application-test.properties")
public class CustomerControllerTest {

    @Autowired
    private MockMvc mvc;

    @InjectMocks
    private CustomerController customerController;

    @Mock
    private CustomerService service;

    @BeforeEach
    public void setup() {
        mvc = MockMvcBuilders.standaloneSetup(customerController)
                .build();
    }

    @Test
    public void getCustomerDataByIdTest() {
        long customerId = 23132;
        String customerName = "BankPol";

        given(service.findFirstByCustomerIdOrderByInfoAsOfDateDesc(customerId))
                .willReturn(createTestCustomerData(customerId, customerName));

        try {
            mvc.perform(get("/api/customer/" + customerId)
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(content()
                            .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("$customerId", is(customerId)).exists())
                    .andExpect(jsonPath("$customerName", is(customerName)).exists());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private CustomerData createTestCustomerData(long customerId, String customerName) {
        return new CustomerData(new Date(),
                customerId,
                customerName,
                new Date(),
                CustomerType.TYPE_A1,
                0.0,
                CustomerRiskClass.A3,
                CustomerBusinessType.BR_8);
    }

}
